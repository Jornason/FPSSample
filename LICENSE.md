# 葫芦历险记

曾经梦想有一种量化技能学习经历能让人沉迷，让自己对量化的不断试错“无法自拔”。

以前自己只能把通过量化赚钱作为自己唯一的目标，但以我个人经验，即便完成了策略研究，回测优化，实盘上架的过程，也不一定立刻能盈利。



<img src="https://geekree-md.oss-cn-shanghai.aliyuncs.com/640.gif"/>



赚钱是需要天时地利人和，有资金入场才会迎来量化的狂欢，大量的闲鱼时间你只能等待。

![20201128221000](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/221047-294081.gif)

人有一种惰性，只有看到利润才有进一步学习的动力，但市场赚钱的时光总是短暂，所以量化学习很少有人能坚持不断完善策略和实盘。





<img src="http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/27/151817-447543.png" alt="image-20201127151814795" style="zoom:50%;" />

机会永远是给有准备的人，遇到好行情，收益曲线就会稳步增长。

![数钱](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/223457-940791.gif)



#### quantclass葫芦体系给了我们一种全新的激励方式：分享输出，赚取葫芦



![001](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/153719-591694.jpeg)



通过不断的学习输出和分享赚取葫芦，让人沉浸在量化的学习中，在量化策略的试错中“不务正业”，经常是写了一天的代码，感觉时间过得好快，陷入深深地空虚和自责中，但就是忍不住“入坑”。



![写一天代码](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/222405-544227.gif)



## 赚取葫芦的主要方式：

1. ### 发原创精华帖
	

	
	![002](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/153742-291627.jpeg)
	
	
	
2. ### ishare做分享
	

	
   ![004](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/153812-327256.jpeg)
	

	
3. ### 每月获得最多点赞
	

	
   ![V5-点赞最多赢金葫芦(1)](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/225005-238856.jpeg)
	

	
	
	
4. ### 积极参与策略研究小组
	

	
   ![006](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/153830-361797.jpeg)
	

	
5. ### 葫芦兑换升级
	

	
   ![003](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/153804-323834.jpeg)
	

	

#### quantclass 有论坛，有微信群，有共研小组，还能赚葫芦激励自己的学习成果，另外葫芦的“价值”也在水涨船高。



![葫芦历险记 V8-葫芦权益(1)](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/214714-599227.jpeg)



#### 你要做的只是沉迷学习无法自拔。

![我心里只有一件事 学习](http://geekree-md.oss-cn-shanghai.aliyuncs.com/typora/202011/28/223049-46945.gif)

